﻿using System;

namespace exercise_18
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = 10;
            var b = 20;
            var c = 30;
            var d = 40;
            var e = 50;

            Console.WriteLine("Part A");
            Console.WriteLine("List of five variables");
            Console.WriteLine("");
            Console.WriteLine($"The value of num1= {a}");
            Console.WriteLine($"The value of num2= {b}");
            Console.WriteLine($"The value of num3= {c}");
            Console.WriteLine($"The value of num4= {d}");
            Console.WriteLine($"The value of num5= {e}");
        }
    }
}
